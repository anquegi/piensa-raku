# Una Gramática para Analizar JSON
# FIXME
# Use of Nil in string context
#  in method cadena at gramática_json.pl6 line 72


grammar Gramática-JSON {
        token TOP        { \s* [ <objeto> | <array> ] \s* }
        rule objeto      { '{' \s*  <listaPar> '}' \s* }
        rule listaPar    { <par> * % \, }
        rule par         { <cadena>':' <valor> }
        rule array       { '[' <listaValor> ']'}
        rule listaValor  {  <valor> * % \, }
        token cadena     { \" ( <[ \w \s \- ' ]>+ ) \" }
        token número     {
                [\+|\-]?
                [ \d+ [ \. \d+ ]? ] | [ \. \d+ ]
                [ <[eE]> [\+|\-]? \d+ ]?
        }
        token valor      {
                | <val=objeto> | <val=array> | <val=cadena> | <val=número>
                | true     | false   | null
        }
}

my $cadena-JSON = q/{
  "nombre": "Juan",
  "apellido": "Duarte",
  "estaVivo": true,
  "edad": 25,
  "direccion": {
    "calle": "21 2nd Street",
    "ciudad": "New York",
    "estado": "NY",
    "codigoPostal": "10021-3100"
  },
  "numerosTelefono": [
    {
      "tipo": "hogar",
      "número": "212 555-1234"
    },
    {
      "tipo": "oficina",
      "número": "646 555-4567"
    },
    {
      "tipo": "movil",
      "número": "123 456-7890"
    }
  ],
  "infantes": [],
  "esposa": null,
  "cuentaBancaria": {
    "credito": 2342.25
  }
}/;

class acciones-JSON {
    method TOP($/) {
        make $/.values.[0].made;
    };
    method objeto($/) {
        make $<listaPar>.made.hash.item;
    }
    method listaPar($/) {
        make $<par>».made.flat;
    }
    method par($/) {
        make $<cadena>.made => $<valor>.made;
    }
    method array($/) {
        make $<listaValor>.made.item;
    }
    method listaValor($/) {
        make [$<valor>.map(*.made)];
    }
    method cadena($/) { make ~$0; }
    method número($/) { make +$/.Str; }
    method valor($/) {
        given ~$/ {
            when "true"  {make Bool::True;}
            when "false" {make Bool::False;}
            when "null"  {make Any;}
            default { make $<val>.made;}
        }
   }
}

my $j-acciones = acciones-JSON.new();
my $coinc = Gramática-JSON.parse($cadena-JSON, :actions($j-acciones));
say $coinc.made;

say "Las claves son: \n", $coinc.made.keys;
say "\nAlgunos Valores:";
say $coinc.made{$_} for <nombre apellido estaVivo>;
say $coinc.made<direccion><ciudad>;
say "\nNúmeros telefónicos:";
say $coinc.made<numerosTelefono>[$_]<tipo número>
    for 0..$coinc.made<numerosTelefono>.end;
