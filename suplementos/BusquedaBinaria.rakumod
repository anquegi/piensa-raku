unit module BusquedaBinaria;

sub bisectar( Str $palabra, @lista_palabras ) is export {
    sub bisectar2( $idx_inferior, $idx_superior ) {
        my $idx_medio = (($idx_inferior + $idx_superior) /2).Int;
        my $encontrada = @lista_palabras[$idx_medio ];
        return $idx_medio if $palabra eq $encontrada;
        return -1 if $idx_inferior >= $$idx_superior;
        if $palabra lt $encontrada {
            # realiza la búsqueda en la primera mitad del array
            return bisectar2 $idx_inferior, $idx_medio - 1;
        }
        else {
            # realiza la búsqueda en la segunda mitad del array
            return bisectar2 $idx_medio+1, $idx_superior;
        }
    }
    my $indice_max = @lista_palabras.end;
    return bisectar2 0, $indice_max;
}

sub mostrar-resultado( Int $result, Str $busqueda ) is export {
    if $result == -1 {
        say "'$busqueda' no encontrada";
    }
    else {
        say "'$busqueda' encontrada: artículo # $result";
    }
}
