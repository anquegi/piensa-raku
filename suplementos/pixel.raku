use v6;

### Class Pixel subclassing class Point2D ###
# English version uses multi methods for different signatures of cambiar_color

class Punto2D {
    has Numeric $.abscisa;	# valor "x"
    has Numeric $.ordenada;	# valor "y"

    method coordenadas {        #  método de acceso para "x" y "y"
        return (self.abscisa, self.ordenada)
    }

    method distancia-al-centro {
        (self.abscisa ** 2 + self.ordenada ** 2) ** 0.5
    }
    method coordenadas-polares {
        my $radio = self.distancia-al-centro;
        my $theta = atan2 self.ordenada, self.abscisa;
        return $radio, $theta;
    }
}

class Pixel is Punto2D {
    has %.color is rw;

    method cambiar_color( %tono ) {
        %!color = %tono
    }
    method cambiar_color2( Int $rojo, Int $verde, Int $azul ) {
        # signatura usando parámetros de posición
        %!color = (rojo => $rojo, verde => $verde, azul => $azul)
    }
}


my $pix = Pixel.new(
        :abscisa(3.3),
        :ordenada(4.2),
        color => {rojo => 34, verde => 233, azul => 145},
);

say "El píxel original tiene los siguientes colores:", $pix.color;

$pix.cambiar_color({:rojo(195), :verde(110), :azul(70),});
say "Colores modificados: ", $pix.color;
say "Nuevas características del píxel:";
printf "\tAbscisa: %.2f\n\tOrdenada: %.2f\n\tColores: R: %d, G: %d, B: %d\n",
       $pix.abscisa, $pix.ordenada,
       $pix.color<rojo>, $pix.color{"verde"}, $pix.color{"azul"};

$pix.cambiar_color2(90, 180, 30);  # argumentos de posición
say "Nuevos colores:
\tR: {$pix.color<rojo>}, G: {$pix.color<verde>}, B: {$pix.color<azul>} ";
