use v6;

### Caculator grammar ###

# use Grammar::Tracer;
my grammar Calculadora {
    rule TOP            { <expr> }
    rule expr           { <término> + % <op-suma-sust> }
    token op-suma-sust  { [< + - >] }
    rule término        { <átomo> + % <op-mult-div> }
    token op-mult-div   { [< * / >] }
    rule átomo {
        | <número>      { make +$<número> }
        | <expr-paren>  { make $<expr-paren>.made }
    }
    rule número         { <signo> ? [\d+ | \d+\.\d+ | \.\d+ ] }
    rule expr-paren     { '(' <expr> ')' }
    token signo         { [< + - >] }
}

class AccionesCalc {
    method TOP ($/) {
        make $<expr>.made
    }
    method expr ($/) {
        $.calcular($/, $<término>, $<op-suma-sust>)
    }
    method término ($/) {
        $.calcular($/, $<átomo>, $<op-mult-div>)
    }
    method expr-paren ($/) {
         make $<expr>.made;
    }
    method calcular ($/, $operandos, $operadores) {
        my $result = (shift $operandos).made;
        while my $op = shift $operadores {
            my $número = (shift $operandos).made;
            given $op {
                when '+' { $result += $número; }
                when '-' { $result -= $número; }
                when '*' { $result *= $número; }
                when '/' { $result /= $número; }
                default  { die "operador desconocido"}
            }
        }
        make $result;
    }
}

for |< 3*4 5/6 3+5 74-32 5+7/3 5*3*2 (4*5) (3*2)+5 4+3-1/5 4+(3-1)/4 >,
    "12 + 6 * 5", " 7 + 12 + 23", " 2 + (10 * 4) ", "3 * (7 + 7)" { 
    my $result = Calculadora.parse($_, :actions(AccionesCalc));
    # say $result;
    printf "%-15s %.3f\n", $/,  $result.made if $result;
}

for "(((2+3)*(5-2))-1)*3", "2 * ((4-1)*((3*7) - (5+2)))"  {
    my $result = Calculadora.parse($_, :actions(AccionesCalc));
    printf "%-30s %.3f\n", $/,  $result.made if $result;
}
