use v6;

### Heap implementation ###

sub construir-mont( @array, $indice is copy ) {
    my $valor-indice = @array[$indice];
    while ($indice) {
        my $padre = Int( ($indice - 1) / 2);
        my $valor-padre = @array[$padre];
        last if $valor-padre lt $valor-indice;
        @array[$indice] = $valor-padre;
        $indice = $padre;
    }
    @array[$indice] = $valor-indice;
}

sub heapify( @array ) {
    for @array.keys -> $i {
        construir-mont @array, $i;
    }
}

sub imprimir-mont( @array ) {
    my $inicio = 0;
    my $final = 0;
    my $último = @array.end;
    my $paso = 1;

    loop {
        say @array[$inicio..$final];
        last if $final == $último;
        $inicio += $paso;
        $paso *= 2;
        $final += $paso;
        $final = $último if $final > $último;
    }
}

sub imprimir-mont2( @array ) {
    my $paso = 0;
        for @array.keys -> $actual {
            my $h_izquierdo = @array[2 * $actual + 1];
        say "$actual\tNodo = @array[$actual];\tNo hijos"
            and next unless defined $h_izquierdo;
        my $h_derecho = @array[2 * $actual + 2] // "' '";

        say "$actual\tNodo = @array[$actual];\tHijos: " ~
            " $h_izquierdo y $h_derecho";
        $paso++;
    }
}


my @entrada =  <m t f l s j p o b h v k n q g r i a d u e c>;
heapify @entrada;
say @entrada;
imprimir-mont @entrada;
imprimir-mont2 @entrada;
