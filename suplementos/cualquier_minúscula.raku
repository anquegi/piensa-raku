use v6;

### Ejercicio 7.2 - cualquier_minúscula 
### - solo dos subrutinas chequean correctamente

sub es-minúscula( Str $char ) {
    return so $char ~~ /^<[a..z]>$/
}

sub cualquier_minúscula1( Str $cadena ){
    for $cadena.comb -> $char {
        if es-minúscula $char {
            return True;
        } else {
            return False;
        }
    }
}

sub cualquier_minúscula2( Str $cadena ) {
    for $cadena.comb -> $char {
        if es-minúscula "char" {
            return True;
        } else {
            return False;
        }
    }
}

sub cualquier_minúscula3( Str $cadena ) {
    my $bandera;
    for $cadena.comb -> $char {
        $bandera = es-minúscula $char;
    }
    return $bandera;
}

sub cualquier_minúscula4( Str $cadena ) {
    my $bandera = False;
    for $cadena.comb -> $char {
        $bandera = $bandera or es-minúscula $char;
    }
    return $bandera;
}

sub cualquier_minúscula5( Str $cadena ) {
    my $bandera = False;
    for $cadena.comb -> $char {
        if es-minúscula $char {
            $bandera = True;
        }
    }
    return $bandera;
}

sub cualquier_minúscula6( Str $cadena ) {
    for $cadena.comb -> $char {
        if es-minúscula $char {
            return 'True';
        }
    }
    return 'False';
}

sub cualquier_minúscula7( Str $cadena ) {
    for $cadena.comb -> $char {
        return True if es-minúscula $char;
    }
    return False;
}

sub cualquier_minúscula8( Str $cadena ) {
    for $cadena.comb -> $char {
        return False unless es-minúscula $char;
    }
    return True;
}

sub cualquier_minúscula9( Str $cadena ) {
    for $cadena.comb -> $char {
        if not es-minúscula $char {
            return False;
        }
    return True;
    }
}


for <FOO bar Baz> -> $str {
    say "1. $str: ", cualquier_minúscula1 $str;
    say "2. $str: ", cualquier_minúscula2 $str;
    say "3. $str: ", cualquier_minúscula3 $str;
    say "4. $str: ", cualquier_minúscula4 $str;
    say "5. $str: ", cualquier_minúscula5 $str;
    say "6. $str: ", cualquier_minúscula6 $str;
    say "7. $str: ", cualquier_minúscula7 $str;
    say "8. $str: ", cualquier_minúscula8 $str;
    say "9. $str: ", cualquier_minúscula9 $str;
}
