# -*- coding: utf-8 -*-
"""
    pygments.styles.slang
    ~~~~~~~~~~~~~~~~~~~~~
    :license: BSD, see LICENSE for details.
"""

from pygments.style import Style
from pygments.token import Keyword, Name, Comment, String, Error, \
    Number, Operator, Whitespace


class SlangStyle(Style):

    background_color = "#ffffff"
    default_style = ''

    styles = {
        Whitespace:            '#ffffff',
        Comment:               '#000000',
        String:                '#487818',
        Number:                '#6A6A6A',
        Operator:              '#AD4D3A',
        Keyword:               'bold #4172A3',
        Keyword.Constant:      '',
        Name.Builtin:          '#7040A0',
        Name.Variable:         'bold #262626',
        Name.Variable.Global:  'bold #262626',
        Error:                 'bg:#e3d2d2 #a61717'
    }
