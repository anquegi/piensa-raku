
\chapter{Introducción}

Bienvenido al arte de la programación de computadoras y al nuevo lenguaje
Raku. Este es uno de los primeros libros que usa Raku,
un lenguaje de programación poderoso, expresivo,
maleable y altamente extensible. Pero este libro es menos acerca de Raku,
y más sobre cómo aprender a escribir programas de computadora. 

Este libro está destinado para principiantes y no requiere ningún conocimiento
previo de programación, aunque espera que aquellos con experiencia de
programación puedan aún así beneficiarse.

\section*{El Objetivo de este Libro}

El objetivo de este libro no es primariamente enseñar Raku, sino enseñar el
arte de la programación, usando el lenguaje Raku. Después de haber completado
el libro, con suerte deberías ser capaz de escribir programas para resolver
problemas relativamente difíciles en Raku, pero mi objetivo principal es
enseñar la ciencia de la computación, la programación de software, y la
resolución de problemas más allá de solamente enseñar el lenguaje Raku por
sí mismo.

Esto significa que no abarcaré cada aspecto de Raku, pero solo un
subconjunto (relativamente grande pero incompleto) del lenguaje. Este libro
no pretende ser una referencia del lenguaje.

No es posible aprender programación o aprender un nuevo
lenguaje de programación solo con leer un libro; la práctica es esencial. Este
libro contiene muchos ejercicios. Se te anima a que hagas un esfuerzo real para
resolverlos. Independiente a si eres capaz de resolver los ejercicios, deberías
mirar las soluciones en el Apéndice, dado que, muy a menudo, se sugieren varias
soluciones con una extensa discusión de la materia y los asuntos relacionados.
Algunas veces, la sección de soluciones del Apéndice introduce ejemplos de
temas que serán cubiertos en el siguiente capítulo---y algunas veces cosas que
no se discutirán en otras partes del libro. Así que, para sacarle provecho a
este libro, te sugiero a que trates de solucionar los ejercicios y revisar las
soluciones e intentarlas.

Hay más de mil ejemplos de código en este libro; estúdialos, asegúrate de
entenderlos, y ejecútalos. Si es posible, trata de cambiarlos y observa que
sucede. Es probable que aprendas mucho de este proceso.


\section*{La Historia de este Libro}

Hay algo muy simple que necesitas saber para entender lo que explico en los
siguientes párrafos. El lenguaje de programación Raku no siempre se ha llamado
Raku. El proyecto comenzó con el nombre Perl~6, dado que fue originalmente
concebido como la siguiente versión de Perl, un sucesor de Perl~5. Sin embargo,
con el pasar del tiempo, el lenguaje ha evolucionado en un lenguaje sintácticamente
diferente a Perl, aunque aún mantiene el espíritu de Perl y avanza los ideales de la
comunidad de Perl. Debido a estas diferencias, en Octubre del 2019 se decidió
cambiar el nombre de Perl~6 a Raku. El libro que está leyendo ahora originalmente se
llamaba \emph{Think Perl~6}. Así que esta nueva edición refleja el cambio de
imagen del lenguaje.

En el transcurso de los últimos tres a cuatro años antes del comienzo del 2016,
he traducido o adaptado al francés un número de tutoriales y artículos acerca de
lo que en aquel entonces se llamaba Perl~6, y también he escrito
algunos artículos totalmente nuevos en francés.~\footnote{Ver por ejemplo 
\url{http://perl.developpez.com/cours/\#TutorielsPerl6}.} Juntos, estos 
documentos representaban para el final del 2015 entre 250 y 300 páginas de
material sobre Raku. Para ese entonces, probablemente había hecho público
más material sobre este nuevo lenguaje en francés que todos los otros escritores
juntos.

A fines del 2015, comencé a pensar que un documento de Raku para principiantes
era algo que faltaba y que estaba dispuesto a llevar a cabo. Busqué alrededor
y encontré que aparentemente no existía nada igual en inglés. Me vino la idea
que, después de todo, sería más útil escribir tal documento inicialmente en
inglés, para dárselo a una audiencia más amplia. Así fue que comencé a 
contemplar escribir una introducción para la programación de Raku destinada
para principiantes. En aquel entonces, mi idea era sobre un tutorial de 50 a
70 páginas y comencé a recopilar material e ideas en esta dirección.

Entonces, algo que cambió mis planes ocurrió.

En Diciembre del 2015, algunos amigos míos estaban contemplando traducir 
al francés \emph{Think Python, Second Edition} de Allen B. Downey\footnote{Ver
\url{http://greenteapress.com/wp/think-python-2e/}.}. Había leído una versión
previa de ese libro excelente y totalmente apoyé la idea de traducirlo\footnote{Lo sé, es
acerca de Python, no Perl o Raku. Sin embargo, no creo en las ``guerras de lenguajes''
y pienso que todos debemos aprender de otros lenguajes; para mí, el lema de Perl,
``hay más de una forma para hacer algo,'' también significa que hacerlo en
Python (o cualquier otro lenguaje) es realmente una posibilidad aceptable.}.
Como resultado, terminé como un co-traductor y el editor técnico de la
traducción al francés de ese libro
\footnote{Ver \url{http://allen-downey.developpez.com/livres/python/pensez-python/}.}.

Mientras trabajaba en la traducción al francés del libro sobre Python
de Allen, se me ocurrió una idea. En lugar de escribir un tutorial
sobre Raku, sería más útil hacer un ``traducción a Raku'' de 
\emph{Think Python}. Debido a que estaba en contacto con Allen en 
el contexto de la traducción al francés, yo le sugerí la idea a Allen, 
quién acogió favorablemente la idea. Así fue como comencé a escribir este 
libro a final de Enero del 2016, poco tiempo después de haber completado
la traducción al francés de su libro sobre Python.

Tuve que usar el nombre ''Perl~6'' en los párrafos anteriores, dado que esa era
la manera en la cual fue llamado en ese punto en la historia. De ahora en
adelante, usaré el nuevo nombre del lenguaje, \emph{Raku}, a menos que me
refiera a eventos prior al cambio de imagen.

De tal manera, este libro es mayormente un derivado de \emph{Think Python}
de Allen, pero adaptado a Raku. Como sucedió, es también algo más que 
una ``traducción a Raku'' del libro de Allen: con suficiente nuevo material,
se ha convertido en un libro totalmente nuevo, con una gran deuda al 
libro de Allen, pero aún un libro nuevo por el cual tomo total responsabilidad.
Cualquier error es mío, no de Allen.

Mi esperanza es que esto será útil para la comunidad de Raku, y generalmente
para la comunidad de código abierto (\emph{open source}) y la comunidad de la
programación de computadoras. En una entrevista, con \emph{LinuxVoice}
(July 2015), Larry Wall, el creador de Raku, dijo: ``Nosotros pensamos que
Perl~6 se podrá aprender como un primer lenguaje''. ¡Esperemos que este libro 
contribuya a lograr este cometido!


\section*{Reconocimientos}

Realmente no sé cómo podría agradecerle a Larry Wall al nivel de gratitud
que él se merece por haber creado Perl en primer lugar, y más recientemente
Raku. ¡Qué seas bendecido por toda la eternidad, Larry!

Y gracias a todos ustedes que fueron parte de esta aventura (en ningún orden en
particular), Tom, Damian, chromatic, Nathan, brian, Jan, Jarkko, John, Johan,
Randall, Mark Jason, Ovid, Nick, Tim, Andy, Chip, Matt, Michael, Tatsuhiko,
Dave, Rafael, Chris, Stevan, Saraty, Malcolm, Graham, Leon, 
Ricardo, Gurusamy, Scott y muchísimos otros.

Todas mis gracias también a aquellos quienes creyeron en el proyecto de Perl~6 y
subsiguientemente Raku, 
y que lo hicieron posible, incluyendo a aquellos que abandonaron en algún
momento pero que contribuyeron por algún tiempo; sé que no fue siempre fácil.

Muchas gracias a Allen Downey, quien amablemente apoyó la idea de adaptar su libro
a este lenguaje y me ayudó en muchos aspectos, pero también se abstuvo de interferir
en lo que yo ponía en este libro.

Le agradezco sinceramente a la gente de O'Reilly quienes aceptaron la idea de 
este libro y sugirieron muchas correcciones o mejoras. Quiero agradecer 
especialmente a Dawn Schanafelt, mi editor de  O'Reilly, cuyos consejos han
contribuido a hacer este un mejor libro. Muchas gracias también a Charles
Roumeliotis, el editor de copia, y Kristen Brown, la editora de producción,
quien arregló muchos problemas tipográficos y faltas ortográficas.

De antemano le doy gracias a todos los lectores quienes ofrecieron comentarios
o enviaron sugerencias o correcciones, al igual que palabras de aliento.

Si ves algo que necesita ser corregido o que podría ser mejorado, por favor
amablemente envía tus comentarios a \url{think.perl6 (at) gmail.com}.

{\bf Nota del traductor}: Alternativamente, puedes enviar tus correos 
electrónicos amablemente a \url{uzluisf (arroba) protonmail.com} si se trata de
algo relacionado directamente con esta traducción.
% ...


\section*{Lista de Contribuciones}

% ...
Me gustaría agradecer especialmente a Moritz Lenz y Elizabeth Mattijsen, 
quienes revisaron los detalles en los borradores de este libro y sugirieron
un sin número de mejoras y correcciones. Liz invirtió mucho tiempo en una
revisión detallada del contenido completo de este libro y le agradezco
eternamente por sus numerosos y muy útiles comentarios. Gracias también a
Timo Paulssen y ryanschoppe quienes también revisaron los primeros borradores
y proveyeron algunos comentarios útiles. Muchísimas gracias también a Uri
Guttman, quien revisó este libro y sugirió un gran número de pequeñas
correcciones y mejoras un poco antes de la publicación.  

Kamimura, James Lenz, y Jeff McClelland cada uno sometieron algunas correcciones
en la lista de errata en el sitio web de O'Reilly. zengargoyle señaló un carácter
falso en un regex y lo arregló en el repositorio Github del libro. zengargoyle
también sugirió una aclaración en el capítulo acerca de la programación
funcional. Otro James (segundo nombre que no conozco) sometió una errata
al sitio web de O'Reilly. Mikhail Korenev sugirió correcciones precisas
para tres fragmento de código. Sébastien Dorey, Jean Forget, y Roland Schmitz 
enviaron algunos correos electrónicos sugiriendo algunas correcciones o 
mejoras útiles. Luis F. Uceta tradujo este libro al español y
corrigió varios errores tipográficos en el repositorio de Github. Gil Magno,
zoffixznet y Joaquin Ferrero también sugirieron varias correcciones en Github.
Threadless-screw arregló un par de referencias defectuosas acerca de los hashes. 
Boyd Duffee encontró dos muestras de código que funcionaban bien cuando las 
escribí, pero que ya no funcionan, debido a cambios en la implementación.
