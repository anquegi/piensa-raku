% Latex source for "Piensa en Raku"
% Copyright (c)  2017  Allen B. Downey and Laurent Rosenfeld.
% License: Creative Commons Attribution-NonCommercial 3.0 Unported License.
% http://creativecommons.org/licenses/by-nc/3.0/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This package takes care of setting up the preamble for the entire document.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{prelude}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    PACKAGES TO LOAD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage[width=5.5in,height=8.5in,hmarginratio=3:2,vmarginratio=1:1]{geometry}
\RequirePackage[T1]{fontenc}
\RequirePackage{textcomp}
\RequirePackage{titlesec}
\RequirePackage[rgb,hyperref,svgname]{xcolor}
\RequirePackage{mathpazo}
\RequirePackage{url}
\RequirePackage{fancyhdr}
\RequirePackage{graphicx}
\RequirePackage{amsmath, amsthm}
\RequirePackage{exercise}
\RequirePackage{makeidx}
\RequirePackage{setspace}
\RequirePackage{upquote}
\RequirePackage{appendix}
\RequirePackage[bookmarks]{hyperref}
\RequirePackage[spanish,english]{babel}
\RequirePackage[mathletters]{ucs}
\RequirePackage[utf8x]{inputenc}
\RequirePackage{fancyvrb}
\RequirePackage{pdfpages}
\RequirePackage{booktabs}
\RequirePackage[outputdir=tmpDir]{minted} % specify dir for minted

%syntax highlighting for code snippets
\newminted{raku}{
xleftmargin=.3in,
tabsize=4,
%style=bw,
style=slang,
}

% define verbatim environment with a left margin
\DefineVerbatimEnvironment{verbatim}{Verbatim}{xleftmargin=.3in}

% Change canned text to spanish
\addto\captionsenglish{\renewcommand{\chaptername}{Capítulo}}
\addto\captionsenglish{\renewcommand{\figurename}{Fig.}}
\addto\captionsenglish{\renewcommand{\contentsname}{Tabla de Contenido}}
\addto\captionsenglish{\renewcommand{\partname}{Parte}}
\addto\captionsenglish{\renewcommand{\appendixname}{Apéndice}}
\addto\captionsenglish{\renewcommand{\indexname}{Índice}}

% Referencing commands
\newcommand{\chaplabel}[1]{\label{chap:#1}}
\newcommand{\Chapref}[1]{Capítulo~\ref{chap:#1}}
\newcommand{\chapref}[1]{Capítulo~\ref{chap:#1}}

\newcommand{\seclabel}[1]{\label{sec:#1}}
\newcommand{\Secref}[1]{Sección~\ref{sec:#1}}
\newcommand{\secref}[1]{Sección~\ref{sec:#1}}

\newcommand{\subseclabel}[1]{\label{subsec:#1}}
\newcommand{\Subsecref}[1]{Subsección~\ref{subsec:#1}}
\newcommand{\subsecref}[1]{Subsección~\ref{subsec:#1}}

\newcommand{\sollabel}[1]{\label{sol:#1}}
\newcommand{\Solref}[1]{Solución:~\ref{sol:#1}}
\newcommand{\solref}[1]{Solución:~\ref{sol:#1}}

\newcommand{\exclabel}[1]{\label{exc:#1}}
\newcommand{\Excref}[1]{Ejercicio~\ref{exc:#1}}
\newcommand{\excref}[1]{Ejercicio~\ref{exc:#1}}

\newcommand{\paglabel}[1]{\label{page:#1}}
\newcommand{\Pagref}[1]{p.~\pageref{page:#1}}
\newcommand{\pagref}[1]{p.~\pageref{page:#1}}

\newcommand{\figlabel}[1]{\label{fig:#1}}
\newcommand{\Figref}[1]{Figura~\ref{fig:#1}}
\newcommand{\figref}[1]{Figura~\ref{fig:#1}}

% Miscellaneous
\newcommand{\engterm}[1]{(\textbf{en.} #1)}
\newcommand{\ordinal}[1]{.\textsuperscript{#1}} % for Spanish ordinals

\sloppy
%\setlength{\topmargin}{-0.375in}
%\setlength{\oddsidemargin}{0.0in}
%\setlength{\evensidemargin}{0.0in}

% Uncomment these to center on 8.5 x 11
%\setlength{\topmargin}{0.625in}
%\setlength{\oddsidemargin}{0.875in}
%\setlength{\evensidemargin}{0.875in}

%\setlength{\textheight}{7.2in}

\setlength{\headsep}{3ex}
\setlength{\parindent}{0.0in}
\setlength{\parskip}{1.7ex plus 0.5ex minus 0.5ex}
\renewcommand{\baselinestretch}{1.02}

% see LaTeX Companion page 62
\setlength{\topsep}{-0.0\parskip}
\setlength{\partopsep}{-0.5\parskip}
\setlength{\itemindent}{0.0in}
\setlength{\listparindent}{0.0in}

% see LaTeX Companion page 26
% these are copied from /usr/local/teTeX/share/texmf/tex/latex/base/book.cls
% all I changed is afterskip

\makeatletter
\renewcommand{\section}{\@startsection
    {section} {1} {0mm}%
    {-3.5ex \@plus -1ex \@minus -.2ex}%
    {0.7ex \@plus.2ex}%
    {\normalfont\Large\bfseries}}
\renewcommand\subsection{\@startsection {subsection}{2}{0mm}%
    {-3.25ex\@plus -1ex \@minus -.2ex}%
    {0.3ex \@plus .2ex}%
    {\normalfont\large\bfseries}}
\renewcommand\subsubsection{\@startsection {subsubsection}{3}{0mm}%
    {-3.25ex\@plus -1ex \@minus -.2ex}%
    {0.3ex \@plus .2ex}%
    {\normalfont\normalsize\bfseries}}

% The following line adds a little extra space to the column
% in which the Section numbers appear in the table of contents
\renewcommand{\l@section}{\@dottedtocline{1}{1.5em}{3.0em}}
\setcounter{tocdepth}{2}
\makeatother

\newcommand{\beforefig}{\vspace{1.3\parskip}}
\newcommand{\afterfig}{\vspace{-0.2\parskip}}

\newcommand{\beforeverb}{\vspace{0.6\parskip}}
\newcommand{\afterverb}{\vspace{0.6\parskip}}

\newcommand{\adjustpage}[1]{\enlargethispage{#1\baselineskip}}


% Note: the following command seems to cause problems for Acroreader
% on Windows, so for now I am overriding it.
%\newcommand{\clearemptydoublepage}{
%            \newpage{\pagestyle{empty}\cleardoublepage}}
\newcommand{\clearemptydoublepage}{\cleardoublepage}

%\newcommand{\blankpage}{\pagestyle{empty}\vspace*{1in}\newpage}
\newcommand{\blankpage}{\vspace*{1in}\newpage}

% HEADERS
\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}{}}

\lhead[\fancyplain{}{\bfseries\thepage}]%
      {\fancyplain{}{\bfseries\rightmark}}
\rhead[\fancyplain{}{\bfseries\leftmark}]%
      {\fancyplain{}{\bfseries\thepage}}
\cfoot{}

\pagestyle{fancyplain}


% Chapter styling (from titlesec)
\newcommand{\darkerblue}{\color[HTML]{6699CC}}
\newcommand{\lighterblue}{\color[HTML]{B3CCE6}}

\titleformat{\chapter}[display]
{\bfseries\huge}
{\filleft\darkerblue{\fontsize{60}{60}\selectfont\thechapter}}
{1.5ex}
{\titlerule
\vspace{1ex}%
\filright}
[\vspace{1ex}%
\titlerule]

% turn off the rule under the header
%\setlength{\headrulewidth}{0pt}

% the following is a brute-force way to prevent the headers
% from getting transformed into all-caps
\renewcommand\MakeUppercase{}

\newtheorem{exercise}{Ejercicio}[chapter]

% Exercise environment
\newtheoremstyle{myex}% name
     {9pt}%      Space above
     {9pt}%      Space below
     {}%         Body font
     {}%         Indent amount (empty = no indent, \parindent = para indent)
     {\bfseries}% Thm head font
     {}%        Punctuation after thm head
     {0.5em}%     Space after thm head: " " = normal interword space;
           %       \newline = linebreak
     {}%         Thm head spec (can be left empty, meaning `normal')

\theoremstyle{myex}


% simple dedication
\newenvironment{dedication}
{
   \cleardoublepage
   \thispagestyle{empty}
   \vspace*{\stretch{1}}
   \hfill\begin{minipage}[t]{0.66\textwidth}
   \raggedright
}%
{
   \end{minipage}
   \vspace*{\stretch{3}}
   \clearpage
}
